# Docker-test_container
FROM ubuntu:18.04

MAINTAINER Shawn Groberg

# Build container
#   docker build -t sgroberg/test_container 

# Start the container with the follonwing command:
#   docker service create  --name test sgroberg/test_container

# Install tftpd-hpa deamon -*1
RUN apt-get update

ENTRYPOINT ls && bash

